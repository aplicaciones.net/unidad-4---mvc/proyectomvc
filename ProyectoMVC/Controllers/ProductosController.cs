﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ProyectoMVC.Models;
using System.Data.SqlClient;

namespace ProyectoMVC.Controllers
{
    public class ProductosController : Controller
    {
        public ActionResult Insertar()
        {
            ModeloProductos obj = new ModeloProductos();
            SqlConnection Conn = new SqlConnection();
            Conn.ConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\ProyectoMVC\\BaseDatos_PoleZone.mdf;Integrated Security=True;Connect Timeout=30";
            Conn.Open();

            obj.IdProd = Convert.ToInt16(Request.Form["htmlIdProd"]);

            String OrdenSql = String.Format("UPDATE productosPoleZone SET stock=stock-1, fechaUltimaVenta=getDate() WHERE Id={0}", obj.IdProd);
            SqlCommand cmd = new SqlCommand(OrdenSql, Conn);
            cmd.ExecuteNonQuery();

            OrdenSql = String.Format("SELECT * from productosPoleZone WHERE Id={0}", obj.IdProd);
            cmd = new SqlCommand(OrdenSql, Conn);
            cmd.ExecuteNonQuery();

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                obj.nomProducto = Convert.ToString(reader[1]);
                obj.stockProd = Convert.ToInt16(reader[2]);
                obj.colorProd = Convert.ToString(reader[3]);
                obj.costoProd = Convert.ToInt16(reader[4]);
                obj.provedorProd = Convert.ToString(reader[5]);
                obj.fechaUltimaVentaProd = Convert.ToString(reader[6]);
            }
            reader.Close();
            Conn.Close();
            return View(obj);
        }

        // GET: Productos
        public ActionResult Index()
        {
            return View();
        }
    }
}