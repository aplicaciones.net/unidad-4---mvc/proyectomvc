﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoMVC.Models
{
    public class ModeloProductos
    {
        public Int16 IdProd
        {
            get;
            set;
        }

        public String nomProducto
        {
            get;
            set;
        }

        public Int16 stockProd
        {
            get;
            set;
        }

        public String colorProd
        {
            get;
            set;
        }

        public Int16 costoProd
        {
            get;
            set;
        }

        public String provedorProd
        {
            get;
            set;
        }

        public String fechaUltimaVentaProd
        {
            get;
            set;
        }
    }
}